# Abris Problem

## Problems

The developer are working with multiple environments - each environment will open/connect to a port. Port has limited 65000.

Solution: Every time developers commit a message -> a new sub domain will get create.

## Ideas

In order to archive that, we need:
- CI-CD pipeline get trigger at specific commit message.
- provisoning tool to install/update/create new subdomain.

## Tech stacks
- Apache (AWS EC2 instance)
- Ansible
- Gitlab CI-CD

## Solution
**Create a new vhost.conf file everytime a specific commit get added and move it to etc/httpd/conf.d/**

Note: I am currently using free tier AWS EC2 with public DNS auto generate. Hence, I am unable to make a subdomain for it ( [You cannot create sub-domain on Amazon's public DNS name](https://serverfault.com/questions/300015/how-to-create-a-subdomain-in-ec2-amazon-com) ). But I believe this would work.

- Get trigger by specific commit by:
```
commit_message_has_release_job:
  only:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^vhost .*$/
  [...]
```
- Create a new vhost.conf by:
```
 - name: Create git_commit dir
      file:
        path: "/var/www/html/{{ git_commit }}"
        state: directory
        owner: "{{ app_user }}"
        mode: '0755'

    - name: Set up Apache virtualHost dinamically
      template:
         src: "files/new_host_apache.conf.j2"
         dest: "/etc/httpd/conf.d/{{ git_commit }}.conf"

  handlers:
    - name: Reload Apache
      service:
        name: httpd
        state: reloaded

    - name: Restart Apache
      service:
        name: httpd
        state: restarted
```

```
ansible-playbook -e "git_commit=..." playbook.yml
```

## Diagrams

![](/Images/abris_3.PNG)

## Screenshoot
![](/Images/abris_1.PNG)
![](/Images/abris_2.PNG)

## What can be done better
- Restrict commit syntax (vhost_a, vhost_b, etc..)
- ansible vault to store secrect within the repo
- Using Hashicorp Vault? - Turn off log.

## Other Solutions?

In case if we working with docker and let's say we want to control trafic between services. Here is one solution  I believe is 1 of the best in the market.

[Nginx Controller Kubernetes](https://medium.com/digitalfrontiers/kubernetes-ingress-with-nginx-93bdc1ce5fa9)

```
 In this case, a DNS record should be created that resolves all DNS queries for my-app.com (like www.my-app.com, services.my-app.com …) to the IP assigned for the Service mentioned above, that points to the NGINX Ingress Controller. Now all requests will be handled by that IP and the NGINX Ingress Controller will perform the routing.
 ```

 ## Test

 - http://34.254.210.248/ - ElasticIP AWS
 - http://34.254.210.248/vhost_abris | http://34.254.210.248/vhost_abris2/ - Subdomain **folder** (Since free tier domain DNS server of aws doesn't allow to generate wildcard - but the folder got created)